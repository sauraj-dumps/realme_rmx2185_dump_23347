## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1609743540423 release-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2185
- Brand: realme
- Flavor: cipher_RMX2185-userdebug
- Release Version: 12
- Id: SQ1A.220105.002
- Incremental: eng.lordsa.20220115.105141
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX2185/RMX2185:11/RP1A.200720.011/1616486375439:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1609743540423-release-keys
- Repo: realme_rmx2185_dump_23347


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
